extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	

func _on_Timer_timeout():
	var delay: float = 0
	var duration: float = 1
	# Configure le fondu pour l'écran
	delay+=duration
	$FadeOut.interpolate_property($Sprite, "modulate:a", 1,-0.3, duration, Tween.TRANS_LINEAR, Tween.EASE_IN,delay)

	$FadeOut.start()
	$FadeOut.repeat=false


func _on_FadeOut_tween_all_completed():
	var _res = get_tree().change_scene_to(load("res://Scenes/Menu.tscn"))
